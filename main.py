import config
from Analyser import Analyser
from FileGrabber import FileGrabber
from MySqlClient import MySqlClient


def run():
    mysql = MySqlClient(config.database)

    queryable_elements = mysql.get_queryable_elements()
    events = mysql.get_events()
    procedures = mysql.get_procedures()
    views = mysql.get_views()

    file_grabber = FileGrabber()
    files = file_grabber.find_files(config.repo_path)

    analyse = Analyser(config.regex)
    analyse.analyse_db_content(events, queryable_elements)
    analyse.analyse_db_content(procedures, queryable_elements)
    analyse.analyse_db_content(views, queryable_elements)

    analyse.analyse_file_content(files, queryable_elements)

    mysql.close_connection()


if __name__ == '__main__':
    run()

import mysql.connector
from mysql.connector import DatabaseError


class MySqlClient:
    def __init__(self, config: dict):
        self._cnx = mysql.connector.connect(**config)

    def get_queryable_elements(self) -> dict:
        cursor = self._cnx.cursor()
        query = ("""WITH table_list AS (SELECT
                                            t.TABLE_SCHEMA AS db_name,
                                            t.TABLE_NAME   AS t_name
                                        FROM
                                            information_schema.TABLES AS t
                                        WHERE
                                            t.TABLE_TYPE = 'BASE TABLE' AND
                                            t.ENGINE != 'ARCHIVE' AND
                                            t.TABLE_SCHEMA NOT IN ('information_schema', 'mysql', 'performance_schema', 'sys')),
                         view_list AS (SELECT v.TABLE_SCHEMA AS db_name, v.TABLE_NAME AS t_name FROM information_schema.VIEWS AS v WHERE v.TABLE_SCHEMA NOT IN ('information_schema', 'mysql', 'performance_schema', 'sys')),
                         combined_list AS (SELECT * FROM table_list AS tl UNION SELECT * FROM view_list AS vl)
                    SELECT *
                    FROM
                        combined_list AS cl
                    ORDER BY
                        cl.db_name,
                        cl.t_name;""")
        cursor.execute(query)

        result = {}
        for (db_name, t_name) in cursor:
            db_element = db_name + '.' + t_name
            result.update({db_element: set()})

        cursor.close()
        return result

    def get_events(self) -> list:
        cursor = self._cnx.cursor()
        query = ("""SELECT
                        t.EVENT_SCHEMA     AS db_name,
                        t.EVENT_NAME       AS ev_name,
                        t.EVENT_DEFINITION AS content
                    FROM
                        information_schema.EVENTS AS t
                    WHERE
                        t.EVENT_SCHEMA NOT IN ('information_schema', 'mysql', 'performance_schema', 'sys');""")

        cursor.execute(query)

        res = []
        for (db_name, ev_name, content) in cursor:
            res.append({'db_name': db_name, 'element_name': ev_name, 'content': content.decode("utf-8")})

        cursor.close()
        return res

    def get_procedures(self) -> list:
        cursor = self._cnx.cursor()
        query = ("""SELECT
                        r.ROUTINE_SCHEMA     AS db_name,
                        r.ROUTINE_NAME       AS r_name,
                        r.ROUTINE_DEFINITION AS content
                    FROM
                        information_schema.ROUTINES AS r
                    WHERE
                        r.ROUTINE_SCHEMA NOT IN ('information_schema', 'mysql', 'performance_schema', 'sys')
                    ORDER BY
                        r.ROUTINE_SCHEMA,
                        r.ROUTINE_NAME;""")

        cursor.execute(query)

        res = []
        for (db_name, r_name, content) in cursor:
            res.append({'db_name': db_name, 'element_name': r_name, 'content': content.decode("utf-8")})

        cursor.close()
        return res

    def get_views(self) -> list:
        cursor = self._cnx.cursor()

        query = ("""SELECT
                        v.TABLE_SCHEMA    AS db_name,
                        v.TABLE_NAME      AS v_name,
                        v.VIEW_DEFINITION AS content
                    FROM
                        information_schema.VIEWS AS v
                    WHERE
                        v.TABLE_SCHEMA NOT IN ('information_schema', 'mysql', 'performance_schema', 'sys')
                    ORDER BY
                        v.TABLE_SCHEMA,
                        v.TABLE_NAME;""")

        cursor.execute(query)

        res = []
        try:
            for (db_name, v_name, content) in cursor:
                res.append({'db_name': db_name, 'element_name': v_name, 'content': content.decode("utf-8")})
        except DatabaseError as e:
            pass

        cursor.close()
        return res

    def close_connection(self):
        self._cnx.close()

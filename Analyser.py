import re


class Analyser:
    def __init__(self, regex_search_string: str):
        self._search_for = regex_search_string

    def analyse_db_content(self, content_list: list, tables: dict):
        for element in content_list:
            db_element = element['db_name'] + '.' + element['element_name']

            for match in re.finditer(self._search_for, element['content'], re.IGNORECASE):
                database = element['db_name']  # No database found .. so use database where db element is located at
                try:
                    if match.group('database') is not None:
                        database = match.group('database')
                except:
                    pass

                try:
                    table = match.group('table')
                except:
                    # No Table found .. for example: only stored procedures are called
                    continue

                current_table = database + '.' + table

                try:
                    tables[current_table].add(db_element)
                except:
                    # Call on an unexisting table
                    tables.update({current_table: set().add('non existing call in ' + db_element)})


    def analyse_file_content(self, content_list: list, tables: dict):
        for element in content_list:
            file_name = element['file']

            for match in re.finditer(self._search_for, element['content'], re.IGNORECASE):
                database = ''
                try:
                    if match.group('database') is not None:
                        database = match.group('database')
                except:
                    pass

                try:
                    table = match.group('table')
                except:
                    # No Table found .. for example: only stored procedures are called
                    continue

                current_table = database + '.' + table

                try:
                    tables[current_table].add(file_name)
                except:
                    # Call on an unexisting table
                    tables.update({current_table: set().add('non existing call in ' + file_name)})

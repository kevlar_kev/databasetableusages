import os


class FileGrabber:

    def find_files(self, rootdir) -> list:
        root_path_length = len(rootdir)
        res = []

        for subdir, dirs, files in os.walk(rootdir):
            for file in files:
                if '.php' in file and 'vendor' not in subdir:
                    file_path = os.path.join(subdir, file)
                    file_path_length = len(file_path)
                    with open(file_path, 'r', encoding="utf-8") as f:
                        data = f.read()

                    res.append({'file': file_path[-(file_path_length - root_path_length - 1):],
                                'content': data})

        return res
